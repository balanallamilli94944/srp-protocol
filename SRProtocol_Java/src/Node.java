import java.io.Serializable;
import java.time.Instant;

public class Node implements Serializable {
    private int data;
    private Status status;
    private Instant receive_localTime;
    private Node next;

    Node(int data){
        this.data = data;
        this.status = null;
        this.receive_localTime = Instant.now();
        this.next = null;
    }

    public int getData() {
        return data;
    }

    public void setData(int data) {
        this.data = data;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Instant getReceive_localTime() {
        return receive_localTime;
    }

    public void setReceive_localTime(Instant receive_localTime) {
        this.receive_localTime = receive_localTime;
    }

    public Node getNext() {
        return next;
    }

    public void setNext(Node next) {
        this.next = next;
    }
}
