
public class Status {

    private boolean ack_status;
    private boolean nack_status;

    public Status(){
        this.ack_status = false;
        this.nack_status = false;
    }


    public boolean isAck_status() {
        return ack_status;
    }

    public void setAck_status(boolean ack_status) {
        this.ack_status = ack_status;
    }

    public boolean isNack_status() {
        return nack_status;
    }

    public void setNack_status(boolean nack_status) {
        this.nack_status = nack_status;
    }

}
