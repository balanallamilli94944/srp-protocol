import java.util.ArrayList;
import java.util.List;

public class Receiver {

    private Status receiverStatus;
    private List<Node> reciever_list;

    Receiver(){
        this.receiverStatus = new Status();
        this.reciever_list = new ArrayList<>();
    }

    public Status ProcessSenderData(List<Node> senderData){
        if(senderData != null || senderData.size() != 0){
            for(Node n : senderData){
                this.reciever_list.add(n);
            }
        }
        this.receiveDataAndSendAckorNack();
        this.showReceivedData();
        return this.receiverStatus;
    }

    private void receiveDataAndSendAckorNack(){
        if(this.reciever_list != null){
            if(this.reciever_list.size() != 0){
                for(Node n : reciever_list){
                    Status curr_status = new Status();
                    curr_status.setAck_status(true);
                    curr_status.setNack_status(false);
                    n.setStatus(curr_status);
                }
                receiverStatus.setAck_status(true);
                receiverStatus.setNack_status(false);
            }else{
                receiverStatus.setAck_status(false);
                receiverStatus.setNack_status(true);
            }
        }else{
            receiverStatus.setAck_status(false);
            receiverStatus.setNack_status(false);
        }
    }

    public void showReceivedData(){
        System.out.println("+++Showing current received data from sender+++");
        for(Node r : reciever_list){
            System.out.println("[+] received ---> "+r.getData());
        }
        System.out.println("sending status");
    }
}
