import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.TimerTask;
import java.util.Date;

public class Sender {

    private List<Node> sending_list;

    public Sender(){
        this.sending_list = new ArrayList<>();
    }

    public void prepareDataToSend(int item_to_be_send){
        Node new_node = new Node(item_to_be_send);
        int data = new_node.getData();
        Instant inst = new_node.getReceive_localTime();
        this.sending_list.add(new_node);
    }

    public List<Node> sendDataToReceiver(){
        return this.sending_list;
    }


}
