import java.util.List;
public class startingPoint {

    public static void main(String args[]) throws InterruptedException {
            //create sender
        Sender s1 = new Sender();
        s1.prepareDataToSend(1);
        s1.prepareDataToSend(2);
        List<Node> nodes = s1.sendDataToReceiver();
        System.out.println("sending data & waiting for 5 secs");
        Thread.sleep(5000);
        //create reciever
        Receiver r1 = new Receiver();
        Status s = r1.ProcessSenderData(nodes);
        if(s.isAck_status()){
            System.out.println("We recieved ack message from receiver");
        }else{
            System.out.println("We received nack message from reciever which means data got corrupted or response timeout happend");
        }
    }

}
